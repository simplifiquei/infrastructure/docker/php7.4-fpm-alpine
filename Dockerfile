FROM --platform=$BUILDPLATFORM php:7.4-fpm-alpine as fpm

WORKDIR /var/www/html

RUN mkdir -p /srv/www /srv/www

RUN apk add tzdata && \
  ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

RUN apk add git bash oniguruma-dev libxml2-dev libzip-dev libressl-dev libressl linux-headers ca-certificates curl $PHPIZE_DEPS
RUN docker-php-ext-install \
        bcmath \
        ctype \
        fileinfo \
        mbstring \
        pdo_mysql \
        zip \
        xml

RUN pecl install mongodb
RUN docker-php-ext-enable mongodb

RUN apk add --no-cache libjpeg-turbo libpng freetype-dev libjpeg-turbo-dev libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

RUN  sed -i 's,^\(MinProtocol[ ]*=\).*,\1'TLSv1.0',g' /etc/ssl/openssl.cnf \
    && sed -i 's,^\(CipherString[ ]*=\).*,\1'DEFAULT@SECLEVEL=1',g' /etc/ssl/openssl.cnf \
    && sed -i '/^default = default_sect/a legacy = legacy_sect' /etc/ssl/openssl.cnf \
    && sed -i '/^\[default_sect\]/a activate = 1' /etc/ssl/openssl.cnf \
    && printf "[legacy_sect]\nactivate = 1" >> /etc/ssl/openssl.cnf \
    && rm -rf /var/lib/apt/lists/*


FROM --platform=$BUILDPLATFORM php:7.4-fpm-alpine as fpm_newrelic

WORKDIR /var/www/html

RUN mkdir -p /srv/www /srv/www

RUN apk add tzdata && \
  ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

RUN apk add git bash oniguruma-dev libxml2-dev libzip-dev libressl-dev libressl linux-headers ca-certificates curl $PHPIZE_DEPS
RUN docker-php-ext-install \
        bcmath \
        ctype \
        fileinfo \
        mbstring \
        pdo_mysql \
        zip \
        xml

RUN pecl install mongodb
RUN docker-php-ext-enable mongodb

RUN apk add --no-cache libjpeg-turbo libpng freetype-dev libjpeg-turbo-dev libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

RUN  sed -i 's,^\(MinProtocol[ ]*=\).*,\1'TLSv1.0',g' /etc/ssl/openssl.cnf \
    && sed -i 's,^\(CipherString[ ]*=\).*,\1'DEFAULT@SECLEVEL=1',g' /etc/ssl/openssl.cnf \
    && sed -i '/^default = default_sect/a legacy = legacy_sect' /etc/ssl/openssl.cnf \
    && sed -i '/^\[default_sect\]/a activate = 1' /etc/ssl/openssl.cnf \
    && printf "[legacy_sect]\nactivate = 1" >> /etc/ssl/openssl.cnf \
    && rm -rf /var/lib/apt/lists/*

# NEW-RELIC
# Install build dependencies.
RUN apk add --no-cache binutils go build-base openssl-dev pcre-dev zlib-dev automake autoconf libtool $PHPIZE_DEPS

# Create working directory.
RUN mkdir /tmp/newrelic && \
    cd /tmp/newrelic

# To add an exception for this directory,
RUN git config --global --add safe.directory /var/www/html

# Get source code, and check out required version.
RUN git clone https://github.com/newrelic/newrelic-php-agent.git .
RUN git checkout v10.14.0.3

# Build extension & move files.
RUN make all
RUN strip --strip-debug agent/modules/newrelic.so && \
    mv agent/modules/newrelic.so $(php-config --extension-dir)/newrelic.so && \
    mv agent/scripts/newrelic.ini.template /usr/local/etc/php/conf.d/docker-php-ext-newrelic.ini && \
    mv bin/daemon /usr/local/bin/newrelic-daemon

RUN rm -R /tmp/newrelic