VERSION ?= 1.0.0

REGISTRY ?= registry.gitlab.com/simplifiquei/infrastructure/docker/php7.4-fpm-alpine

# Commands
docker: docker-build docker-push

docker-build:
	docker build . --platform=linux/arm64 -t ${REGISTRY}:${VERSION}

docker-push:
	docker push ${REGISTRY}:${VERSION}
